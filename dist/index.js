"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const sum = (a, b) => a + b;
const app = express_1.default();
app.get("/", (req, res) => {
    console.log(sum(5, 5));
    res.send("Hello");
});
app.listen(5000, () => console.log("Server running"));
