export const DB_PATH_PROD: string =
  "mongodb+srv://dar_levy:Dardar08@pearls-jam-db-atlas.qq6xn.mongodb.net/pearls-jam-db-atlas?retryWrites=true&w=majority";
export const SERVER_PORT: any = process.env.SERVER_PORT || 5000;
export const GET_ALL_FROM_DB_PATH: string = "/api/concerts";
export const LOG_FILE_PATH: string = "defaultLogFile.log";
export const UNHANDLED_EX_LOG_PATH: string = "unhandledExceptions.log";
export const SWAGGER_API_DOCS_PATH = "/api-docs";

export const IS_DEV_ENV: boolean = false;
export const DB_PATH_TEST: string = "mongodb://localhost/pearl-jam-db-test";
export const START_TIME_TEST: string = "January 01 2015";
export const END_TIME_TEST: string = "January 01 2022";
export const LOCATION_TEST: string = "UAE";
