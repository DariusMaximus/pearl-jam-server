import Concert from "../models/concert";
import IConcert from "../interfaces/concert";

export const getConcertsByTime = async (
  starttime: string,
  endtime: string
): Promise<IConcert[]> => {
  const startTime: Date = new Date(starttime);
  const endTime: Date = new Date(endtime);
  return await Concert.find().then((arr: IConcert[]) =>
    arr.filter(
      (concert: any) => concert.date > startTime && concert.date < endTime
    )
  );
};

export const getConcertsByLocation = async (
  location: string
): Promise<IConcert[]> => {
  return await Concert.find().then((arr: IConcert[]) =>
    arr.filter((concert: any) => concert.location === location)
  );
};
