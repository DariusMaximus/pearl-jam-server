import { Document } from "mongoose";

export default interface IConcert extends Document {
  location: string;
  date: Date;
}
