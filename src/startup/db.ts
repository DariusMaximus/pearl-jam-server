// @ts-ignore
import winston from "winston";
import mongoose from "mongoose";
import * as config from "../config/config";

export default function connectToDb() {
  const db: string = config.IS_DEV_ENV
    ? config.DB_PATH_TEST
    : config.DB_PATH_PROD;
  mongoose
    .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => winston.log(`Connected to ${db}...`))
    .catch((ex) => winston.error(ex));
}
