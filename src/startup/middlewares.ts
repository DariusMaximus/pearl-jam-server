import express from "express";
import cors from "cors";
import swaggerUi from "swagger-ui-express";
import concerts from "../routes/api";
import { error } from "../middlewares/error";
import * as config from "../config/config";
import * as swaggerDocs from "../swagger/swagger.json";

export const useAllMiddlewares = (app: any) => {
  app.use(express.json());
  app.use(cors());
  app.use(
    config.SWAGGER_API_DOCS_PATH,
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocs)
  );
  app.use(config.GET_ALL_FROM_DB_PATH, concerts);
  app.use(error);
};
