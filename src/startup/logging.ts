// @ts-ignore
import winston from "winston";
import "express-async-errors";
import * as config from "../config/config";

export function logging() {
  winston.handleExceptions(
    new winston.transports.File({ filename: config.UNHANDLED_EX_LOG_PATH })
  );

  process.on("unhandledRejection", (ex) => {
    throw ex;
  });

  winston.add(winston.transports.File, { filename: config.LOG_FILE_PATH });
}
