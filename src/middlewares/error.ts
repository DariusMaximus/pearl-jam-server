// @ts-ignore
import winston from "winston";
import { Request, Response, NextFunction } from "express";

export const error = (
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
): void => {
  winston.error(err.message);
  res.status(500).send("Something went wrong, Please talk with Dar Levy");
};
