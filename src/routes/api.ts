import express, { Request, Response, Router } from "express";
import * as controller from "../controllers/concerts";

const router: Router = express.Router();

router.get("/", async (req: Request, res: Response) => {
  try {
    if (req.query.starttime && req.query.endtime) {
      const concerts = await controller.getConcertsByTime(
        req.query.starttime.toString(),
        req.query.endtime.toString()
      );

      return res.status(200).json(concerts);
    } else if (req.query.location) {
      const concerts = await controller.getConcertsByLocation(
        req.query.location.toString()
      );

      res.status(200).send(concerts);
    } else {
      res.status(400).send("Bad request");
    }
  } catch (ex) {
    res.status(500).send(ex.message);
  }
});

export = router;
