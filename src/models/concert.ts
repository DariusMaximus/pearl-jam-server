import mongoose, { Schema } from "mongoose";
import IConcert from "../interfaces/concert";

const ConcertSchema: Schema = new Schema({
  location: { type: String, required: true },
  date: { type: Date, required: true },
});

export default mongoose.model<IConcert>("Concert", ConcertSchema);
