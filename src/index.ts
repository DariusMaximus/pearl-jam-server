// @ts-ignore
import winston from "winston";
import express, { Application } from "express";
import { useAllMiddlewares } from "./startup/middlewares";
import { logging } from "./startup/logging";
import connectToDb from "./startup/db";
import { SERVER_PORT } from "./config/config";

const app: Application = express();

logging();
useAllMiddlewares(app);
connectToDb();

const server = app.listen(SERVER_PORT, () =>
  winston.log(`Listening on port ${SERVER_PORT}`)
);

export default server;
