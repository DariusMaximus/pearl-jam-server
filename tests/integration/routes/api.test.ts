import request from "supertest";
import Concert from "../../../src/models/concert";
import server from "../../../src/index";
import {
  START_TIME_TEST,
  END_TIME_TEST,
  LOCATION_TEST,
} from "../../../src/config/config";

describe("/api/concerts", () => {
  beforeAll(async () => {
    await Concert.collection.insertMany([
      { location: "Israel", date: new Date("OCT. 10 2019") },
      { location: "UAE", date: new Date("OCT. 10 2018") },
    ]);
  });
  afterAll(async () => {
    server.close();
    await Concert.remove({});
  });

  describe("GET ?starttime&&endtime", () => {
    it("should return concerts in time range by valid time query", async () => {
      const res = await request(server).get(
        `/api/concerts?starttime=${START_TIME_TEST}&&endtime=${END_TIME_TEST}`
      );

      expect(res.status).toBe(200);
      expect(res.body.length).toBe(2);
      expect(res.body.some((s: any) => s.location === "Israel")).toBeTruthy();
      expect(res.body.some((s: any) => s.location === "UAE")).toBeTruthy();
    });

    it("should return 400 status by invalid time query", async () => {
      const res = await request(server).get(`/api/concerts?starttime`);

      expect(res.status).toBe(400);
    });

    it("should return 404 status by invalid route", async () => {
      const res = await request(server).get(`/api?starttime`);

      expect(res.status).toBe(404);
    });
  });

  describe("GET ?location", () => {
    it("should return concerts in location by valid location query", async () => {
      const res = await request(server).get(
        `/api/concerts?location=${LOCATION_TEST}`
      );

      expect(res.status).toBe(200);
      expect(res.body.length).toBe(1);
      expect(res.body.some((s: any) => s.location === "UAE")).toBeTruthy();
    });

    it("should return 400 status by invalid location query", async () => {
      const res = await request(server).get(`/api/concerts?location`);

      expect(res.status).toBe(400);
    });

    it("should return 404 status by invalid route", async () => {
      const res = await request(server).get(`/api?location`);

      expect(res.status).toBe(404);
    });
  });
});
